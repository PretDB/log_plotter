import datetime
import pandas
from pyexpat import model
import django
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseNotAllowed, HttpResponseNotFound
from django.shortcuts import redirect, render, get_object_or_404
from django.template import loader
from django.views import generic
from django import forms
import django.db.models

import base64
from io import BytesIO
from matplotlib.figure import Figure
import re

from soupsieve import match

from .models import Plot, Template, ValueType, Captures
from .forms import PlotForm, SimplePlotFileForm

# Create your views here.

def http_method_list(methods):
    def http_methods_decorator(func):
        def function_wrapper(self, request, **kwargs):
            methods = [method.upper() for method in methods]
            if not request.method.upper() in methods:
                return HttpResponse(status=405) # not allowed

            return func(self, request, **kwargs)
        return function_wrapper
    return http_methods_decorator

def index(request):
    return render(request, 'index.html')

def template_index(request):
    if request.method == 'GET':
        latest_templates = Template.objects.order_by('-updated_at')[:5]
        context = {
            'latest_templates': latest_templates
        }
        return render(request, 'templates/index.html', context)
        pass
    elif request.method == 'POST':
        return render(request, 'templates/create.html')
        pass
    pass

def template_detail(request, template_id):
    template = get_object_or_404(Template, pk=template_id)
    context = {
        'template': template
    }
    if request.method == 'GET':
        return render(request, 'templates/detail.html', context)
    elif request.method == 'PUT':
        if 'template_name' in request.POST.keys() and not request.POST['template_name'] == '':
            template.template_name = request.POST['template_name']
            pass

        if 'test_str' in request.POST.keys() and not request.POST['test_str'] == '':
            template.default_test_str = request.POST['test_str']
            pass

        if 'regex_str' in request.POST.keys() and not request.POST['regex_str'] == '':
            template.regex_str = request.POST['regex_str']
            pass

        matches = re.match(template.regex_str, template.default_test_str)
        error_message = None
        captures = []
        # if matches is not None:
        #     for k, v in request.POST.items():
        #         if k.startswith('capture-'):
        #             capture_type = k.split('-')[1]
        #             capture = Captures()
        #             capture.value_type = get_object_or_404(ValueType, type_str=v)
        #             capture.value_name = capture_type
        #             captures.append(capture)
        #             pass
        #         pass
        #     template.captures_set.update(captures)
        #     pass
        # else:
        #     error_message = '无法匹配测试字符串'
        #     pass
        value_types = ValueType.objects.all()
        context['error_message'] = error_message
        context['matches'] = matches.groupdict().items() if matches is not None else None
        context['value_types'] = value_types
        context['captures'] = template.captures_set.all()
        template.save()
        return render(request, 'templates/detail.html', context)
        pass
    else:
        return HttpResponseNotAllowed()


    template = get_object_or_404(Template, pk=template_id)
    matches = re.match('^(?P<val1>\d+)\s+(?P<val2>\d+)$', '123 233')
    if matches is not None:
        matches = matches.groupdict()
        pass
    context = {
        'template': template,
        'matches': matches
    }
    return render(request, 'templates/detail.html', context)

def create_template(request):
    pass

def template_update(request, template_id):
    new_reg_str = request.POST['regex_str']
    new_test_str = request.POST['test_str']
    new_template_name = request.POST['template_name']

    template = get_object_or_404(Template, pk=template_id)
    template.regex_str = new_reg_str
    template.default_test_str = new_test_str
    template.template_name = new_template_name

    matches = re.match(new_reg_str, new_test_str)
    error_message = None if matches is not None else '无法匹配测试字符串'
    value_types = ValueType.objects.all()
    context = {
        'template': template,
        'error_message': error_message, 
        'matches': matches.groupdict().items() if matches is not None else None,
        'value_types': value_types
    }
    return render(request, 'templates/detail.html', context)

def value_type(request, value_type_id):
    response = f"正在查看捕获[{value_type_id}]"
    return HttpResponse(response)

def plot_index(request):
    latest_plots = Plot.objects.order_by('-updated_at')[:10]
    template = loader.get_template('plots/index.html')
    context = {
        'latest_plots': latest_plots,
    }
    return render(request, 'plots/index.html', context)

class TemplateMatcher:
    def __init__(self, template: Template):
        self.reg = re.compile(template.regex_str)
        default_match_res = self.reg.match(template.default_test_str)
        if default_match_res is None:
            raise "默认测试字符串不匹配"
            pass
        # name => convert_rule
        self.captures = {}
        pass

def plot_detail(request, plot_id):
    plot = get_object_or_404(Plot, pk=plot_id)
    fig = Figure()
    ax = fig.subplots()
    ax.plot([1, 2])
    buf = BytesIO()
    fig.savefig(buf, format='png')
    data = base64.b64encode(buf.getbuffer()).decode('ascii')
    context = {
        'plot': plot,
        'img_data': data
    }
    return render(request, 'plots/detail.html', context)

def do_plot(data_frames: dict) -> str:
    figure = Figure(figsize=(24, 15))
    axes = figure.subplots(len(data_frames))
    for i, key in enumerate(data_frames):
        if data_frames[key] is None:
            continue
        for col in data_frames[key].columns:
            axes[i].plot(data_frames[key].index, data_frames[key][col], label=col)
            pass
        axes[i].legend()
        axes[i].grid()
        pass
    buf = BytesIO()
    figure.savefig(buf, format='png')
    data = base64.b64encode(buf.getbuffer()).decode('ascii')
    return data

def load_plot_file_form_into_dataframe(file: InMemoryUploadedFile):
    REG_VM_REC = re.compile(r'^(?P<time>\d{8}T\d{6}\.\d{6})\d{3} VmPeak=\[(?P<vmpeak>\d+)\], VmSize=\[(?P<vmsize>\d+)\], VmRSS=\[(?P<vmrss>\d+)\], VmData=\[(?P<vmdata>\d+)\], VmStk=\[(?P<vmstk>\d+)\], VmExe=\[(?P<vmexe>\d+)\], VmLib=\[(?P<vmlib>\d+)\], VmSwap=\[(?P<vmswap>\d+)\]')
    REG_TC_REC = re.compile(r'^(?P<time>\d{8}T\d{6}\.\d{6})\d{3} allocated=\[(?P<allocated>\d+)\], ,heap_size=\[(?P<heap_size>\d+)\], ,page_free=\[(?P<page_free>\d+)\], ,unmapped=\[(?P<unmapped>\d+)\]')
    vm_recs = []
    tc_recs = []
    file.open()
    while file.readable():
        line = file.readline().decode('ascii')
        if line == '':
            break
        vm_match = REG_VM_REC.match(line)
        if vm_match is not None:
            vm_recs.append({
                'time': datetime.datetime.strptime(vm_match.group('time'), '%Y%m%dT%H%M%S.%f'),
                'vmpeak': int(vm_match.group('vmpeak')),
                'vmsize': int(vm_match.group('vmsize')),
                'vmrss': int(vm_match.group('vmrss')),
                'vmdata': int(vm_match.group('vmdata')),
                'vmstk': int(vm_match.group('vmstk')),
                'vmexe': int(vm_match.group('vmexe')),
                'vmlib': int(vm_match.group('vmlib')),
                'vmswap': int(vm_match.group('vmswap'))
            })
            pass
        tc_match = REG_TC_REC.match(line)
        if tc_match is not None:
            tc_recs.append({
                'time': datetime.datetime.strptime(tc_match.group('time'), '%Y%m%dT%H%M%S.%f'),
                'allocated': int(tc_match.group('allocated')),
                'heap_size': int(tc_match.group('heap_size')),
                'page_free': int(tc_match.group('page_free')),
                'unmapped': int(tc_match.group('unmapped'))
            })
            pass
        pass
    file.close()
    vm_df = pandas.DataFrame(vm_recs)
    tc_df = pandas.DataFrame(tc_recs)
    vm_df = vm_df.set_index('time') if not vm_df.empty else None
    tc_df = tc_df.set_index('time') if not tc_df.empty else None
    return vm_df, tc_df

def plot_delete(request, plot_id: int):
    plot = get_object_or_404(Plot, pk=plot_id)
    plot.delete()

    return redirect('plot_index')
    pass

def plot_form(request, plot_id: int = None):
    if request.method == 'POST':
        form = PlotForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            vm_df, tc_df = load_plot_file_form_into_dataframe(request.FILES['data_file'])
            dataframes = {
                'vm': vm_df,
                'tc': tc_df
            }
            figure_data = do_plot(dataframes)
            context = {
                'form': form,
                'figure_data': figure_data,
            }
            return render(request, 'plots/form.html', context)
            pass
        else:
            return HttpResponseBadRequest("Invalid form postd")
        pass
    elif request.method == 'GET':
        if plot_id is None:
            form = PlotForm()
            return render(request, 'plots/form.html', {'form': form})
            pass
        else:
            plot = get_object_or_404(Plot, pk=plot_id)
            form = PlotForm(instance=plot)
            updated_at = plot.updated_at
            vm_df, tc_df = load_plot_file_form_into_dataframe(plot.data_file)
            dataframes = {
                'vm': vm_df,
                'tc': tc_df
            }
            figure_data = do_plot(dataframes)
            context = {
                'plot_instance': plot,
                'form': form,
                'updated_at': updated_at,
                'figure_data': figure_data
            }
            return render(request, 'plots/form.html', context)
            pass
        pass
    elif request.method == 'DELETE':
        if plot_id is not None:
            plot = get_object_or_404(Plot, pk=plot_id)
            plot.delete()
            pass
        else:
            return HttpResponseBadRequest()
        pass
    else:
        return HttpResponseNotAllowed()
    pass

def help(request):
    return render(request, 'help.html')
    pass