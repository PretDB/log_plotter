from re import template
from statistics import mode
from . import models

from django.contrib import admin

# Register your models here.
admin.site.register(models.Template)
admin.site.register(models.ValueType)
admin.site.register(models.Plot)