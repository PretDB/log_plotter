from django import forms

from .models import Plot

class SimplePlotFileForm(forms.Form):
    file = forms.FileField()
    pass

class PlotForm(forms.ModelForm):
    class Meta:
        model = Plot
        fields = ('plot_name', 'data_file')
        pass
    pass