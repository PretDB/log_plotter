# Generated by Django 4.0.3 on 2022-03-04 07:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('log_plotter', '0010_alter_valuetype_convert_rule'),
    ]

    operations = [
        migrations.AlterField(
            model_name='valuetype',
            name='convert_rule',
            field=models.CharField(default='raise "Value Cast Not Provided"', help_text='值转换方式，具体实现为：</br>\n        <pre>\n        def convert(raw_val: str):\n            val = {your convert code}\n            return val\n        </pre>\n        例如int类型可以填入“<pre>int(raw_val)</pre>”</br>\n        某时间戳类型（20210123T124512）可以填入\n        “<pre>import datetime; datetime.datetime.strptime(raw_val, "%Y%m%dT%H%M%S"</pre>”</br>\n        又或者浮点类型可以使用“float(raw_val)”\n        ', max_length=500),
        ),
    ]
