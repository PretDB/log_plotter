# Generated by Django 4.0.3 on 2022-03-04 07:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('log_plotter', '0014_template_template_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='template',
            name='template_name',
            field=models.CharField(db_index=True, default='未命名模板', max_length=200, verbose_name='模板名称'),
        ),
        migrations.AlterField(
            model_name='valuetype',
            name='type_str',
            field=models.CharField(db_index=True, max_length=20, unique=True, verbose_name='值名字'),
        ),
    ]
