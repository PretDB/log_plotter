# Generated by Django 4.0.3 on 2022-03-04 08:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('log_plotter', '0019_rename_name_plot_plot_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='plot',
            name='created_at',
            field=models.DateTimeField(auto_created=True, auto_now=True, verbose_name='创建时间'),
        ),
    ]
