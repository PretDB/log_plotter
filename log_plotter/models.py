from distutils.command.upload import upload
from email.policy import default
from enum import auto
from hashlib import sha1
from unicodedata import name
import uuid
from django.db import models
from django.db.models.signals import pre_delete, pre_save
from django.contrib.auth.models import User

import re

from django.dispatch import receiver
from django.forms import FileField

# Create your models here.

class ValueType(models.Model):
    type_str = models.CharField("值名字", max_length=20, null=False, unique=True, db_index=True)
    convert_rule = models.CharField("转换规则", max_length=500,\
        default='raise "Value Cast Not Provided"',\
        help_text="""值转换方式</br>
        例如int类型可以填入“<pre>int({})</pre>”</br>
        某时间戳类型（20210123T124512）可以填入
        “<pre>__import__('datetime').datetime.strptime({}, '%Y%m%dT%H%M%S')</pre>”</br>
        又或者浮点类型可以使用“<pre>float({})</pre>”
        """)

    def __str__(self) -> str:
        return self.type_str
    pass

class Template(models.Model):
    template_name = models.CharField("模板名称", max_length=200, default="未命名模板", db_index=True)
    regex_str = models.CharField("正则表达试", max_length=200, null=False)
    default_test_str = models.CharField("默认测试字符串", max_length=200)
    updated_at = models.DateTimeField("创建时间", auto_now=True, auto_now_add=False, auto_created=True)

    def __str__(self) -> str:
        return f'{self.template_name} --- {self.regex_str}'
    
    pass

class Captures(models.Model):
    template_name = models.ForeignKey(Template, on_delete=models.CASCADE, null=True)
    value_type = models.ForeignKey(ValueType, on_delete=models.CASCADE, null=True)
    value_name = models.CharField("捕获名", max_length=200, null=False, blank=False, default="")

    def __str__(self) -> str:
        return f'{self.value_name} --- {str(self.value_type)}'
        pass
    pass

def plot_dir_by_userid(instance, filename: str=None):
    import os.path
    # use hashed file name
    # import hashlib
    # new_filename = None
    # ctx = hashlib.sha256()
    # if instance.data_file.multiple_chunks():
    #     for data in instance.data_file.chunks(2048):  # default chunk size
    #         ctx.update(data)
    #         pass
    #     pass
    # else:
    #     ctx.update(instance.data_file.read())
    # new_filename = f'{ctx.hexdigest()}'
    return os.path.join("plots", filename)
    pass

class Plot(models.Model):
    plot_name = models.CharField("图表名", max_length=200, default="未命名图表", db_index=True)
    data_file = models.FileField("数据源文件", upload_to=plot_dir_by_userid)
    updated_at = models.DateTimeField("更新时间", auto_now=True, auto_now_add=False, auto_created=True, db_index=True)

    # plot_file_name = models.FilePathField("图表文件名", allow_files=True, allow_folders=False, default=None, editable=False, auto_created=True)

    def __str__(self) -> str:
        import os.path
        return f'{self.plot_name} --- ({str(os.path.basename(self.data_file.name))})'
    pass

def file_deleter(file: models.fields.files.FieldFile):
    if file is None:
        return
    storage = file.storage
    path = file.path
    storage.delete(path)
    pass

@receiver(pre_delete, sender=Plot)
def plot_file_deleter(instance: Plot, **kwargs):
    instance.data_file.delete()
    pass

@receiver(pre_save, sender=Plot)
def plot_file_updater(instance: Plot, **kwargs):
    if instance.data_file.name is not None:
        rows = Plot.objects.filter(id=instance.id)
        if rows.exists() and not rows[0].data_file.name == instance.data_file.name:
            file_deleter(rows[0].data_file)
            pass
        pass
    pass
