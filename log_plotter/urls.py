"""tools_site URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from . import views

from django.contrib import admin
from django.urls import path

urlpatterns = [
    path('', views.index, name='index'),
    path('help', views.help, name='help'),
    path('plots/', views.plot_index, name='plot_index'),
    path('plot/', views.plot_form, name='plot_detail'),
    path('plot/<int:plot_id>/', views.plot_form, name='plot_detail'),
    path('plot/<int:plot_id>/delete', views.plot_delete, name='plot_delete'),
    path('template/', views.template_index, name='template_index'),
    path('template/<int:template_id>/', views.template_detail, name='template_detail'),
]
